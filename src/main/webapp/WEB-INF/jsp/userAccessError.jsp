<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Access error</title>
    <style>
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
        }

        .hor {
            background-color: #333;
        }

        .acc {
            background-color: dimgray;
        }

        li {
            float: left;
        }

        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }

        li a:hover {
            background-color: #111;
        }

        .home {
            background: red;
        }
    </style>
</head>
<body>
<ul class="hor">
    <li><a class="home" href="home">Home</a></li>
    <li style="float: right"><a href="">Login</a></li>
    <li style="float: right"><a href="signup">Sign Up</a></li>
</ul>

<h2 style="text-align: center; margin-top: 150px">
    <strong>To continue, you need to <a href="login">login</a> </strong>
</h2>
</body>
</html>
