<%@ page import="entity.User" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Account</title>
    <style>
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
        }

        .hor {
            background-color: #333;
        }

        .add {
            background-color: dimgray;
        }

        li {
            float: left;
        }

        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }

        li a:hover {
            background-color: #111;
        }

        .home {
            background: red;
        }
    </style>
</head>
<body>
<% ResourceBundle bundle = ResourceBundle.getBundle("language", new Locale("ru")); %>
<ul class="hor">
    <li><a class="home" href="home"><%=bundle.getString("ui.account")%></a></li>
    <li><a href="account"><%=bundle.getString("ui.account")%></a></li>
    <li style="float: right"><a href="logout"><%=bundle.getString("ui.logout")%></a></li>
    <li style="float: right; color: white; margin-right: 10px"> <%=bundle.getString("ui.welcome")%>, ${sessionScope.user.firstName}!</li>
</ul>

<ul class="add">
    <li><a href=""><%=bundle.getString("ui.info")%></a></li>
    <li><a href="account/bids_serv"><%=bundle.getString("ui.my_bids")%></a></li>
    <li><a href="account/create_bid"><%=bundle.getString("ui.create_bid")%></a></li>
</ul>

<h1 style="alignment: center">
    <% User user = (User) request.getSession().getAttribute("user");%>

    <%=bundle.getString("ui.welcome")%>, <%= user.getFirstName()%>!

    <br><br>
    Full stats:
    <br>
    Number: <%= user.getPhone() %>
    <br>
    Name: <%= user.getFirstName() + " " + user.getLastName()%>
    <br>
</h1>
</body>
</html>
