<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>All bids</title>
    <style>
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
        }

        .hor {
            background-color: #333;
        }

        li {
            float: left;
        }

        a {
            display: block;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }

        li a {
            color: white;
        }

        th a {
            color: #111111;
        }

        li a:hover, .selected {
            background-color: #111;
        }

        .home {
            background: red;
        }

        .tab {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        .tab td, .tab th {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: center;
        }

        .tab tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        .tab tr:hover {
            background-color: #ddd;
        }

        .filter {
            background: burlywood;
        }

    </style>
</head>
<body>
<ul class="hor">
    <li><a class="home" href="../manager_serv">Active bids</a></li>
    <li><a href="">All bids</a></li>
    <li style="float: right"> <a href="../logout">Logout</a> </li>
    <li style="float: right; color: white; margin-right: 10px"> Manager access mode</li>
</ul>

<form class="filter" action="manager_all_serv" method="post">
    <ul>
        <li> From:</li>
        <li style="margin-left: 3px">
            <label>
                <select name="from">
                    <option value="0">All</option>
                    <option value="1">Dnipro</option>
                    <option value="2">Kyiv</option>
                    <option value="3">Kharkiv</option>
                    <option value="4">Lviv</option>
                    <option value="5">Odessa</option>
                </select>
            </label>
        </li>
        <li> To:</li>
        <li style="margin-left: 3px">
            <label>
                <select name="to">
                    <option value="0">All</option>
                    <option value="1">Dnipro</option>
                    <option value="2">Kyiv</option>
                    <option value="3">Kharkiv</option>
                    <option value="4">Lviv</option>
                    <option value="5">Odessa</option>
                </select>
            </label>
        </li>
        <li> Status:</li>
        <li style="margin-left: 3px">
            <label>
                <select name="status">
                    <option value="any">All</option>
                    <option value="open">Open</option>
                    <option value="validated">Waits for payment</option>
                    <option value="paid">Paid</option>
                    <option value="delivered">Delivered</option>
                    <option value="canceled">Canceled</option>
                </select>
            </label>
        </li>
        <li> Type:</li>
        <li style="margin-left: 3px">
            <label>
                <select name="type">
                    <option value="any">All</option>
                    <option value="regular">Regular</option>
                    <option value="fragile">Fragile</option>
                    <option value="insured">Insured</option>
                </select>
            </label>
        </li>
        <li><input type="submit" value="Sort"></li>
    </ul>
</form>

<table class="tab">
    <h2 style="text-align: center">
        Bids:
    </h2>
    <tr>
        <th>From</th>
        <th> To</th>
        <th> Status</th>
        <th> Type</th>
        <th> Created date</th>
        <th> Delivery date</th>
        <th> Total price</th>
    </tr>
    <c:forEach items="${requestScope.mBids}" var="bid">
        <tr>
            <td>${bid.fromDirection.city}</td>
            <td>${bid.toDirection.city}</td>
            <c:choose>
                <c:when test="${bid.status.ordinal == '0'}">
                    <td>open</td>
                </c:when>
                <c:when test="${bid.status.ordinal == '1'}">
                    <td style="color: dodgerblue">waits for payment</td>
                </c:when>
                <c:when test="${bid.status.ordinal == '2'}">
                    <td style="color: orange">paid</td>
                </c:when>
                <c:when test="${bid.status.ordinal == '3'}">
                    <td style="color: limegreen">delivered</td>
                </c:when>
                <c:when test="${bid.status.ordinal == '4'}">
                    <td style="color: red">canceled</td>
                </c:when>
            </c:choose>
            <td>${bid.type}</td>
            <td>${bid.createdDate}</td>
            <td>${bid.deliveryDate}</td>
            <td>${bid.totalPrice}
                <c:if test="${bid.status.ordinal == '0'}">
                    <form action="manager_serv" method="post">
                        <input type="hidden" name="validate" value="${bid.id}">
                        <input type="submit" value="Validate">
                    </form>
                </c:if>
            </td>
        </tr>
    </c:forEach>
</table>

<nav style="background: dimgray">
    <ul>
        <c:if test="${requestScope.currentPage != 1}">
            <li>
                <a href="manager_all_serv?page=${requestScope.currentPage-1}">Previous</a>
            </li>
        </c:if>

        <c:forEach begin="1" end="${requestScope.pagesCount}" var="i">
            <c:choose>
                <c:when test="${requestScope.currentPage eq i}">
                    <li class="selected">
                        <a>${i}</a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li>
                        <a href="manager_all_serv?page=${i}">${i}</a>
                    </li>
                </c:otherwise>
            </c:choose>
        </c:forEach>

        <c:if test="${requestScope.currentPage lt requestScope.pagesCount}">
            <li>
                <a href="manager_all_serv?page=${requestScope.currentPage+1}">Next</a>
            </li>
        </c:if>
    </ul>
</nav>
</body>
</html>
