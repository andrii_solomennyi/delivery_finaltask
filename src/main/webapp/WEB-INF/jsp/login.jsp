<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <style>
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
        }

        .hor {
            background-color: #333;
        }

        li {
            float: left;
        }

        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }

        li a:hover {
            background-color: #111;
        }

        .home {
            background: red;
        }
    </style>
</head>
<body>
<% ResourceBundle bundle = ResourceBundle.getBundle("language", new Locale("ru")); %>
<ul class="hor">
    <li><a class="home" href="home"><%=bundle.getString("ui.home")%></a></li>
    <li style="float: right"><a href="login"><%=bundle.getString("ui.login")%></a></li>
    <li style="float: right"><a href="signup"><%=bundle.getString("ui.signup")%></a></li>
</ul>
<form action="login_ver" method="post">
    <h1><%=bundle.getString("ui.welcome")%>!</h1>
    <hr>
    <p style="color: red">
        <%
            String loginError = request.getParameter("le");
            if (loginError == null) {
                loginError = "";
            } else {
                loginError = "Invalid phone number or password";
            }
        %>
        <%=loginError%>
    </p>
    <p>
        <label>
            <input type="tel" placeholder="<%=bundle.getString("login.phone")%>" name="phone" required>
        </label>
    </p>
    <p>
        <label>
            <input type="password" placeholder="<%=bundle.getString("login.password")%>" name="password" required>
        </label>
    </p>

    <button type="submit"><%=bundle.getString("login.submit")%></button>
    <hr>
    <p><%=bundle.getString("login.account")%> <a href=signup><%=bundle.getString("signup.submit")%>.</a></p>
</form>
</body>
</html>
