<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Rates</title>
    <style>
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
        }

        .hor {
            background-color: #333;
        }

        .acc {
            background-color: dimgray;
        }

        li {
            float: left;
        }

        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }

        li a:hover {
            background-color: #111;
        }

        .home {
            background: red;
        }

        .tab {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        .tab td, .tab th {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: center;
        }

        .tab tr:nth-child(even){background-color: #f2f2f2;}

        .tab tr:hover {background-color: #ddd;}
    </style>
</head>
<body>
<ul class="hor">
    <li><a class="home" href="../home">Home</a></li>
    <c:choose>
        <c:when test="${sessionScope.user != null}">
            <li><a href="../account">Account</a></li>
            <li style="float: right"><a href="../logout">Logout</a></li>
            <li style="float: right; color: white; margin-right: 10px"> Welcome, ${sessionScope.user.firstName}!</li>
        </c:when>
        <c:otherwise>
            <li style="float: right"><a href="../login">Login</a></li>
            <li style="float: right"><a href="../signup">Sign Up</a></li>
        </c:otherwise>
    </c:choose>
</ul>

<ul class="acc">
    <li><a href="directions_serv">Directions</a></li>
    <li><a href="">Rates</a></li>
    <li><a href="calculator">Calculator</a></li>
</ul>

<h3 align="center">Weight</h3>
<table class="tab">
    <tr>
        <th>Under</th>
        <c:forEach items="${requestScope.weights}" var="w">
            <td>${w.maxWeight}</td>
        </c:forEach>
    </tr>
    <tr>
        <th>Price</th>
        <c:forEach items="${requestScope.weights}" var="w">
            <td>${w.price}</td>
        </c:forEach>
    </tr>
</table>
<br>
<br>
<h3 align="center">Volume</h3>
<table class="tab">
    <tr>
        <th>Box size (h*l*w)</th>
        <c:forEach items="${requestScope.volumes}" var="v">
            <td>${v.height}x${v.length}x${v.width}</td>
        </c:forEach>
    </tr>
    <tr>
        <th>Price</th>
        <c:forEach items="${requestScope.volumes}" var="v">
            <td>${v.price}</td>
        </c:forEach>
    </tr>
</table>
<br>
<br>
<h3 align="center">Distance</h3>
<table class="tab">
    <tr>
        <th>Under</th>
        <c:forEach items="${requestScope.distances}" var="d">
            <td>${d.maxDistance}</td>
        </c:forEach>
    </tr>
    <tr>
        <th>Price</th>
        <c:forEach items="${requestScope.distances}" var="d">
            <td>${d.price}</td>
        </c:forEach>
    </tr>
</table>
</body>
</html>
