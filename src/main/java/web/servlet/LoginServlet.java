package web.servlet;

import entity.User;
import enums.Role;
import exceptions.ErrorPageException;
import web.DAO.UserDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * TODO: add PRG pattern
 */

@WebServlet("/login_ver")
public class LoginServlet extends HttpServlet {
    UserDAO userDAO = UserDAO.getInstance();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if(userDAO.validateUser(request.getParameter("phone"), request.getParameter("password"))){

            User user;
            try {
                user = userDAO.getUserByPhone(request.getParameter("phone"));
            } catch (ErrorPageException e) {
                response.sendRedirect("/delivery_war/error");
                return;
            }

            HttpSession session = request.getSession();
            session.setAttribute("user", user);

            if(user.getRole() == Role.user) {
                try {
                    request.getRequestDispatcher("/account").forward(request, response);
                } catch (ServletException e) {
                    response.sendRedirect("/delivery_war/error");
                }
            } else if (user.getRole() == Role.manager){
                try {
                    request.getRequestDispatcher("/manager_serv?get=1").forward(request, response);
                } catch (ServletException e) {
                    response.sendRedirect("/delivery_war/error");
                }
            }
        } else {
            response.sendRedirect("/delivery_war/login?le=e");
        }

    }
}
