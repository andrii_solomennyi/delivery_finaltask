<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SignUp</title>
</head>
<body>
<form action="signup_ver" method="post">
    <h1>SignUp</h1>
    <hr>

    <p style="color: red">
        <%
            String phoneError = request.getParameter("phe");
            if (phoneError == null) {
                phoneError = "";
            } else if (phoneError.equals("ipn")) {
                phoneError = "Invalid phone number";
            } else if (phoneError.equals("pae")) {
                phoneError = "This phone number is already registered!";
            }

            System.out.println(phoneError);
        %>
        <%= phoneError %>
    </p>
    <p>
        <label>
            <input type="tel" placeholder="Enter phone number" name="phone" required>
        </label>
    </p>

    <p style="color: red">
        <%
            String nameError = request.getParameter("ne");
            if (nameError == null) {
                nameError = "";
            } else {
                nameError = "Invalid name";
            }
        %>
        <%= nameError %>
    </p>
    <p>
        <label>
            <input type="text" placeholder="Enter first name" name="fname" required>
        </label>
        <label>
            <input type="text" placeholder="Enter last name" name="lname" required>
        </label>
    </p>

    <p style="color: red">
        <%
            String passwordError = request.getParameter("pe");
            if (passwordError == null) {
                passwordError = "";
            } else {
                passwordError = "Password must be 8+ symbols long, contain only latin characters and any symbols";
            }
        %>
        <%= passwordError %>
    </p>
    <p>
        <label>
            <input type="password" placeholder="Enter password" name="password" required>
        </label>
    </p>

    <p style="color: red">
        <%
            String rPasswordError = request.getParameter("rpe");
            if (rPasswordError == null) {
                rPasswordError = "";
            } else {
                rPasswordError = "Passwords dont match";
            }
        %>
        <%= rPasswordError %>
    </p>
    <p>
        <label>
            <input type="password" placeholder="Re-enter password" name="rpassword" required>
        </label>
    </p>

    <button type="submit">SignUp</button>

    <hr>
    <p>Already have an account? <a href=login>Sign in.</a></p>
</form>
</body>
</html>
