<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<form action="login_ver" method="post">
    <h1>Welcome!</h1>
    <hr>
    <p style="color: red">
        <%
            String loginError = request.getParameter("le");
            if (loginError == null) {
                loginError = "";
            } else {
                loginError = "Invalid phone number or password";
            }
        %>
        <%=loginError%>
    </p>
    <p>
        <label>
            <input type="tel" placeholder="Enter phone number" name="phone" required>
        </label>
    </p>
    <p>
        <label>
            <input type="password" placeholder="Enter Password" name="password" required>
        </label>
    </p>

    <button type="submit">Login</button>
</form>
</body>
</html>
