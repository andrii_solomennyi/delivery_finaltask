<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error</title>
</head>
<body>
<%
    String errorMessage = request.getParameter("error");
    if (errorMessage == null) {
        errorMessage = "Unknown error";
    } else if (errorMessage.equalsIgnoreCase("nue")) {
        errorMessage = "Null user error!";
    } else if (errorMessage.equalsIgnoreCase("mae")) {
        errorMessage = "Manager access error";
    }
%>
<h1 style="color: red">
    <%= errorMessage%>
</h1>
</body>
</html>
