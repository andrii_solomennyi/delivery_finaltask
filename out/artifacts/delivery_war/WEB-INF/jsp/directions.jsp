<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored = "false" %>
<html>
<head>
    <title>Directions</title>
    <style>
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
        }

        .hor {
            background-color: #333;
        }

        .acc {
            background-color: dimgray;
        }

        li {
            float: left;
        }

        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }

        li a:hover {
            background-color: #111;
        }

        .home {
            background: red;
        }

        .tab {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        .tab td, .tab th {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: center;
        }

        .tab tr:nth-child(even){background-color: #f2f2f2;}

        .tab tr:hover {background-color: #ddd;}
    </style>
</head>
<body>
<% ResourceBundle bundle = ResourceBundle.getBundle("language", new Locale("ru")); %>
<ul class="hor">
    <li><a class="home" href="../home"><%=bundle.getString("ui.home")%></a></li>

    <c:choose>
        <c:when test="${sessionScope.user != null}">
            <li><a href="../account"><%=bundle.getString("ui.account")%></a></li>
            <li style="float: right"><a href="../logout"><%=bundle.getString("ui.logout")%></a></li>
            <li style="float: right; color: white; margin-right: 10px"> <%=bundle.getString("ui.welcome")%>, ${sessionScope.user.firstName}!</li>
        </c:when>
        <c:otherwise>
            <li style="float: right"><a href="../login"><%=bundle.getString("ui.login")%></a></li>
            <li style="float: right"><a href="../signup"><%=bundle.getString("ui.signup")%></a></li>
        </c:otherwise>
    </c:choose>
</ul>

<ul class="acc">
    <li><a href=""><%=bundle.getString("ui.directions")%></a></li>
    <li><a href="rates_serv"><%=bundle.getString("ui.rates")%></a></li>
    <li><a href="calculator"><%=bundle.getString("ui.calculator")%></a></li>
</ul>

<table class="tab">
    <tr>
        <th><%=bundle.getString("ui.name")%></th>
    </tr>

    <c:forEach items="${requestScope.directions}" var="d">
        <tr>
            <td>${d.city}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
