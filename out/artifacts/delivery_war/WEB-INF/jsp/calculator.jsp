<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Calculator</title>
    <style>
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
        }

        .hor {
            background-color: #333;
        }

        .acc {
            background-color: dimgray;
        }

        li {
            float: left;
        }

        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }

        li a:hover {
            background-color: #111;
        }

        .home {
            background: red;
        }
    </style>
</head>
<body>
<% ResourceBundle bundle = ResourceBundle.getBundle("language", new Locale("ru")); %>
<ul class="hor">
    <li><a class="home" href="../home"><%=bundle.getString("ui.home")%></a></li>
    <c:choose>
        <c:when test="${sessionScope.user != null}">
            <li><a href="../account"><%=bundle.getString("ui.account")%></a></li>
            <li style="float: right"><a href="../logout"><%=bundle.getString("ui.logout")%></a></li>
            <li style="float: right; color: white; margin-right: 10px"> <%=bundle.getString("ui.welcome")%>, ${sessionScope.user.firstName}!</li>
        </c:when>
        <c:otherwise>
            <li style="float: right"><a href="../login"><%=bundle.getString("ui.login")%></a></li>
            <li style="float: right"><a href="../signup"><%=bundle.getString("ui.sighup")%></a></li>
        </c:otherwise>
    </c:choose>
</ul>

<ul class="acc">
    <li><a href="../home/directions_serv"><%=bundle.getString("ui.directions")%></a></li>
    <li><a href="../home/rates_serv"><%=bundle.getString("ui.rates")%></a></li>
    <li><a href=""><%=bundle.getString("ui.calculator")%></a></li>
</ul>

<form action="calculator_serv" method="post" style="align-self: center">
    <h3>
        <%=bundle.getString("calculator.departure")%>:
    </h3>
    <p>
        <label>
            <select name="departure">
                <option value="1">Dnipro</option>
                <option value="2">Kyiv</option>
                <option value="3">Kharkiv</option>
                <option value="4">Lviv</option>
                <option value="5">Odessa</option>
            </select>
        </label>
    </p>

    <h3>
        <%=bundle.getString("calculator.destination")%>:
    </h3>
    <p>
        <label>
            <select name="destination">
                <option value="1">Dnipro</option>
                <option value="2">Kyiv</option>
                <option value="3">Kharkiv</option>
                <option value="4">Lviv</option>
                <option value="5">Odessa</option>
            </select>
        </label>
    </p>

    <h3>
        <%=bundle.getString("calculator.weight")%>:
        <%
            String answer = "";
            if(request.getParameter("price") != null){
                answer = bundle.getString("calculator.answer") + request.getParameter("price");
            }
        %>

        <tag style="margin-left: 500px">
            <%= answer %>
        </tag>
    </h3>
    <p>
        <label>
            <input type="number" placeholder="<%=bundle.getString("calculator.weight")%>" name="weight" step="any" min="0.1" max="100" required>
        </label>
    </p>

    <h3>
        Volume:
    </h3>
    <p>
        <label>
            <select name="volume">
                <option value="1"><%=bundle.getString("calculator.small_box")%> (10, 24, 17)</option>
                <option value="2"><%=bundle.getString("calculator.low_box")%> (5, 34, 24)</option>
                <option value="3"><%=bundle.getString("calculator.medium_box")%> (10, 34, 24)</option>
                <option value="4"><%=bundle.getString("calculator.big_box")%> (12, 37, 18)</option>
            </select>
        </label>
    </p>
    <input type="submit" placeholder="<%=bundle.getString("ui.submit")%>">
</form>
</body>
</html>
